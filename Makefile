DOCKER_COMPOSE = docker-compose
PHP_CMD = php


# This will stop all containers services
dcomp-stop:
	@echo "\n==> Stopping docker container"
	$(DOCKER_COMPOSE) stop

# This will remove all containers services
dcomp-down:
	@echo "\n==> Removing docker container"
	$(DOCKER_COMPOSE) down

# Start the containers and build them every time this commands is called
dcomp-up:
	@echo "\n==> Docker container building and starting ..."
	$(DOCKER_COMPOSE) up --build -d

