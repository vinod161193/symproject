<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase {

    public function invokeTest()
    {
        $this->assertPageTitleSame('1', '1');
        $this->assertContains("a", "abc");
    }
}

